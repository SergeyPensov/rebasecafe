package com.rebaseCaffe.model;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class Dish {
    private final String title;
    private final String description;
    private final BigDecimal price;

}
